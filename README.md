# SpTest

Smart pension test, this is my solution to the test. 

## Installation

build the gem in the repo:

    $ gem build sp_test.gemspec

install it yourself as:

    $ gem install ./sp_test-0.1.0.gem

and then:

    $ bundle install

## Usage

To run tests, rubocop and simplecov use `rake`

coverage is populated in 
`coverage/index.html`

if installation went correctly then the command `sp_test` will run the script.

if the installation went wrong then another way to run the script is to use 
the 
command use 
`bin/sp_test`, 
this was written on windows 
so possible issue is executable permissions will not be carried over so if 
this is the case in linux `chmod+x` will make the script executable.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
