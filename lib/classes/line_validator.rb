# frozen_string_literal: true

# Validator for lines from the log file
class LineValidator
  def self.validate(line)
    /.+ \d{3}\.\d{3}.\d{3}\.\d{3}/.match?(line)
  end
end
