# frozen_string_literal: true

require "set"
# View Calculator does the calculations for the most views and the most
# unique views
class ViewCalculator
  def self.views(array_of_webpages)
    hash_of_webpages = webpage_ip_addresses_to_hash(array_of_webpages)
    hash_of_webpages.to_h do |key, value|
      value.is_a?(Array) ? [key, value.size] : [key, 1]
    end
  end

  def self.uniq_views(array_of_webpages)
    hash_of_webpages = webpage_ip_addresses_to_hash(array_of_webpages)
    hash_of_webpages.to_h do |key, value|
      value.is_a?(Array) ? [key, value.to_set.size] : [key, 1]
    end
  end

  private_class_method def self.webpage_ip_addresses_to_hash(array)
    hashes = array.map { |pair| Hash[*pair] }
    hashes.inject do |hash1, hash2|
      hash1.merge(hash2) do |*a|
        a[1, 2]
      end
    end
  rescue NoMethodError
    raise ViewCalculatorException
  end
end
