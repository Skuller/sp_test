# frozen_string_literal: true

# Webpage List Output converts the hashes to ordered list and outputs to sp_test
class WebpageListOutput
  def self.order(webpages_views_hash)
    webpages_views_hash.sort_by { |_web, views| views }.reverse!
  rescue NoMethodError
    raise WebpageListOutputException
  end

  def self.output(array_of_webpages_and_views, views_or_unique_view)
    raise WebpageListOutputException, "Not Views or Unique Views" if
      views_or_unique_view != "Views" && views_or_unique_view != "Unique Views"
    raise WebpageListOutputException, "Not an Array" unless
      array_of_webpages_and_views.is_a?(Array)

    array_of_webpages_and_views.each do |web_views|
      puts("Webpage: #{web_views[0]}  #{views_or_unique_view}: #{web_views[1]}")
    end
  end
end
