# frozen_string_literal: true

# WebpageParser parses a log file with webpages and ip address and returns an
# array of them
class WebpageParser
  def self.parse(file)
    ary_webpages_ip_addresses = []
    File.foreach(file) do |line|
      raise(WebpageParserException) unless LineValidator.validate(line)

      ary_webpages_ip_addresses.append(line.split)
    end
    ary_webpages_ip_addresses
  rescue SystemCallError
    raise WebpageParserException
  end
end
