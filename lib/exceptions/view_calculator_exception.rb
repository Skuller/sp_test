# frozen_string_literal: true

# An exception that is thrown when the array is not provided properly
class ViewCalculatorException < StandardError
  def initialize(msg = "Array name not provided or malformed")
    super
  end
end
