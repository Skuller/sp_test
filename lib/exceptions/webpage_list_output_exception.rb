# frozen_string_literal: true

# An exception that is thrown when the hash is not provided properly
class WebpageListOutputException < StandardError
  def initialize(msg = "Hash not provided or malformed")
    super
  end
end
