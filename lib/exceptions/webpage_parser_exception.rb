# frozen_string_literal: true

# An exception that is thrown when the filename is not provided properly
class WebpageParserException < StandardError
  def initialize(msg = "File name not provided or file malformed")
    super
  end
end
