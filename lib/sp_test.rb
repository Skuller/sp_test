# frozen_string_literal: true

require_relative "sp_test/version"
require_relative "sp_test/file_location"
require_relative "classes/webpage_parser"
require_relative "classes/view_calculator"
require_relative "classes/webpage_list_output"
require_relative "classes/line_validator"
require_relative "exceptions/webpage_parser_exception"
require_relative "exceptions/view_calculator_exception"
require_relative "exceptions/webpage_list_output_exception"

# SpTest converts log of webpages and ip address to ordered list of webpages
# and views
module SpTest
  def self.run(file = SpTest::FILE_LOCATION)
    webpage_hash = WebpageParser.parse(file)
    views = WebpageListOutput.order(ViewCalculator.views(webpage_hash))
    uniq_views = WebpageListOutput.order(ViewCalculator.uniq_views(webpage_hash))
    WebpageListOutput.output(views, "Views")
    WebpageListOutput.output(uniq_views, "Unique Views")
  end
end
