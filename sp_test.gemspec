# frozen_string_literal: true

require_relative "lib/sp_test/version"

Gem::Specification.new do |spec|
  spec.name          = "sp_test"
  spec.version       = SpTest::VERSION
  spec.authors       = ["James Kavanagh"]
  spec.email         = ["jkav0872@gmail.com"]

  spec.summary       = "Gem that provides a log parser for Smart Pension test."
  spec.homepage      = "https://gitlab.com/Skuller/sp_test"
  spec.license       = "MIT"
  spec.required_ruby_version = ">= 3.0.2"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{\Abin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_development_dependency "sp_test", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
