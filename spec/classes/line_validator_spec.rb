# frozen_string_literal: true

require "rspec"

RSpec.describe LineValidator do
  context "when a line is retrieved from the file" do
    it "should return true if the line meets webpage route space ip address" do
      line = "/help_page/1 126.318.035.038"
      expect(LineValidator.validate(line)).to be_truthy
    end
    it "should return false if the line does not meets webpage route space ip address" do
      line = "/help_page/1126.318.035.038"
      expect(LineValidator.validate(line)).to be_falsey
    end
  end
end
