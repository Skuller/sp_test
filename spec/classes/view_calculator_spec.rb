# frozen_string_literal: true

require "rspec"
# rubocop:disable Metrics/BlockLength
RSpec.describe ViewCalculator do
  before do
    @array_of_webpages = [%w[/help_page/1 126.318.035.038],
                          %w[/contact 184.123.665.067],
                          %w[/home 184.123.665.067],
                          %w[/about/2 444.701.448.104],
                          %w[/help_page/1 929.398.951.889],
                          %w[/contact 184.123.665.067]]
  end

  context "when an array of webpages and IP addresses is provided to views" do
    it "should return a hash of each webpage and views" do
      res = described_class.views(@array_of_webpages)
      expect(res).to include("/help_page/1")
      expect(res).to include("/contact")
      expect(res).to include("/home")
      expect(res).to include("/about/2")
      expect(res).to include(match("/help_page/1") => 2)
      expect(res).to include(match("/contact") => 2)
      expect(res).to include(match("/home") => 1)
    end
  end

  context "when an array of webpages and IP addresses is provided to
    uniq_views" do
    it "should return a hash of each webpage with unique  views" do
      res = described_class.uniq_views(@array_of_webpages)
      expect(res).to include("/help_page/1")
      expect(res).to include("/help_page/1")
      expect(res).to include("/contact")
      expect(res).to include("/home")
      expect(res).to include("/about/2")
      expect(res).to include(match("/help_page/1") => 2)
      expect(res).to include(match("/contact") => 1)
      expect(res).to include(match("/home") => 1)
    end
  end

  context "when an array is not passed to views and uniq_views" do
    it "should throw an exception" do
      expect { described_class.views("") }.to raise_error(ViewCalculatorException)
      expect { described_class.uniq_views("") }.to raise_error(ViewCalculatorException)
    end
  end
end
# rubocop:enable Metrics/BlockLength
