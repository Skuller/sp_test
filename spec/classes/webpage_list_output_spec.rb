# frozen_string_literal: true

require "rspec"

# rubocop:disable Metrics/BlockLength
RSpec.describe WebpageListOutput do
  before do
    @hash_of_webpages_views = { "/help_page/1" => 2, "/contact" => 2,
                                "/home" => 3 }
    @hash_of_webpages_uniq_views = { "/help_page/1" => 2, "/contact" => 1, "/home" => 3 }
    @ordered_array_webpages_views = [["/home", 3], ["/contact", 2],
                                     ["/help_page/1", 2]]
  end

  context "when the hash of the webpages and views is passed to order" do
    it "returns an ordered array" do
      res = described_class.order(@hash_of_webpages_views)
      result = described_class.order(@hash_of_webpages_uniq_views)
      expect(res).to match_array [["/home", 3], ["/contact", 2],
                                  ["/help_page/1", 2]]
      expect(result).to match_array [["/home", 3], ["/help_page/1", 2],
                                     ["/contact", 1]]
    end
  end

  context "when the ordered array is passed to output" do
    it "should output the ordered array with views to the sp_test" do
      expect do
        described_class.output(@ordered_array_webpages_views, "Views")
        # rubocop:disable Layout/LineLength
      end.to output("Webpage: /home  Views: 3\nWebpage: /contact  Views: 2\nWebpage: /help_page/1  Views: 2\n").to_stdout
      # rubocop:enable Layout/LineLength
    end
    it "should output the ordered array with unique views to the sp_test" do
      expect do
        described_class.output(@ordered_array_webpages_views, "Unique Views")
        # rubocop:disable Layout/LineLength
      end.to output("Webpage: /home  Unique Views: 3\nWebpage: /contact  Unique Views: 2\nWebpage: /help_page/1  Unique Views: 2\n").to_stdout
      # rubocop:enable Layout/LineLength
    end
    it "should throw an exception if second parameter is not Views or Unique Views" do
      expect { described_class.output(@ordered_array_webpages_views, "") }.to raise_error(WebpageListOutputException)
    end
  end

  context "when the hash is not passed to output" do
    it "should throw an exception" do
      expect { described_class.output("", "Views") }.to raise_error(WebpageListOutputException)
    end
  end
end
# rubocop:enable Metrics/BlockLength
