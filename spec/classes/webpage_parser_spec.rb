# frozen_string_literal: true

require "rspec"
require "tempfile"

RSpec.describe WebpageParser do
  context "when a file is provided" do
    it "returns an array of webpages and ip addresses" do
      @file = Tempfile.new("test_data")
      @file.write("/help_page/1 126.318.035.038
/contact 184.123.665.067
/home 184.123.665.067
/about/2 444.701.448.104
/help_page/1 929.398.951.889")
      @file.rewind
      expect(described_class.parse(@file.path)).to contain_exactly(
        %w[/help_page/1 126.318.035.038],
        %w[/contact 184.123.665.067],
        %w[/home 184.123.665.067],
        %w[/about/2 444.701.448.104],
        %w[/help_page/1 929.398.951.889]
      )
      @file.close
      @file.unlink
    end
  end

  context "when a file is not provided" do
    it "should throw an exception" do
      expect { described_class.parse("") }.to raise_error(WebpageParserException)
    end
  end
end
