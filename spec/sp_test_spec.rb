# frozen_string_literal: true

RSpec.describe SpTest do
  it "has a version number" do
    expect(SpTest::VERSION).not_to be nil
  end

  it "has a file connect to FILE_LOCATION" do
    expect(SpTest::FILE_LOCATION).not_to be nil
  end

  context "When a log file is provided" do
    it "should output to the sp_test a list of the webpages and views" do
      file = "./spec/test_data/webserver.log"
      expect { SpTest.run(file) }.to output(<<~OUTPUT).to_stdout
        Webpage: /help_page/1  Views: 2
        Webpage: /about  Views: 2
        Webpage: /index  Views: 2
        Webpage: /about/2  Views: 2
        Webpage: /home  Views: 2
        Webpage: /contact  Views: 2
        Webpage: /help_page/1  Unique Views: 2
        Webpage: /about  Unique Views: 2
        Webpage: /index  Unique Views: 2
        Webpage: /about/2  Unique Views: 2
        Webpage: /home  Unique Views: 2
        Webpage: /contact  Unique Views: 2
      OUTPUT
    end
  end
end
